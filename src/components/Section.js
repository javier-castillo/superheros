import "./Section.css";

const Section = ({name, children}) => {
    return ( 
        <div className='section'>
            <span className='section-name'>{name}: </span>
            <span className="section-content">{children}</span>
        </div>
     );
}
 
export default Section;