import { Card } from 'antd';

const { Meta } = Card;

const HeroImage = ({ name, src }) => {
    return ( 
        <Card
            hoverable
            cover={<img alt={name} src={src} />}
        >
            <Meta title={name} />
        </Card>
     );
}
 
export default HeroImage;