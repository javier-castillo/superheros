import { Card } from 'antd';
import Powerstat from './Powerstat';

const HeroPowerStats = ({
    combat,
    durability,
    intelligence,
    power,
    speed,
    strength
}) => {
    return ( 
        <Card title="Powerstats" hoverable>
            <Powerstat name="Combat" value={combat} />
            <Powerstat name="Durability" value={durability} />
            <Powerstat name="Intelligence" value={intelligence} />
            <Powerstat name="Speed" value={speed} />
            <Powerstat name="Strength" value={strength} />
            <Powerstat name="Power" value={power} />
        </Card>
     );
}
 
export default HeroPowerStats;