import { Pagination } from "antd";

const Navigation = ({ current, total, onChange }) => {
    return ( 
        <Pagination
            simple
            current={current}
            total={total}
            onChange={onChange}
            defaultPageSize={1}
        />
    );
}
 
export default Navigation;