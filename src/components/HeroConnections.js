import { Card } from 'antd';
import Section from "./Section";

const HeroConnections = ({ groupAffiliation, relatives }) => {
    return (
        <Card title="Connections" hoverable>
            <Section name="Group Affiliation">{groupAffiliation}</Section>
            <Section name="Relatives">{relatives}</Section>
        </Card>
    );
}
 
export default HeroConnections;