import { Card, Space, Tag } from 'antd';
import Section from "./Section";

const HeroBiography = ({
        aliases, 
        alignment, 
        alterEgos, 
        firstAppearance, 
        fullName, 
        placeOfBirth, 
        publisher
    }) => {
    
    return ( 
        <Card title="Biography" hoverable>
            <Section name="Full Name">{fullName}</Section>
            <Section name="Place of Birth">{placeOfBirth}</Section>
            <Section name="Aliases">
                <Space size={[0, 8]} wrap>
                    {aliases.map(alias => <Tag>{alias}</Tag>)}
                </Space>
            </Section>
            <Section name="Alignment">
                <Tag color={alignment === 'good' ? 'cyan': 'red'}>{alignment}</Tag>
            </Section>
            <Section name="First Appearance">{firstAppearance}</Section>
            <Section name="Alter Egos">{alterEgos}</Section>
            <Section name="Publisher">{publisher}</Section>
        </Card>
     );
}
 
export default HeroBiography;