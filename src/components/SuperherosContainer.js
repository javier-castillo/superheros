import React, { useState } from 'react';
import { Col, Row, Space } from 'antd';
import Navigation from './Navigation';
import HeroImage from "./HeroImage";
import HeroBiography from './HeroBiography';
import HeroAppearance from './HeroAppearance';
import HeroPowerStats from './HeroPowerStats';
import HeroConnections from './HeroConnections';
import "./SuperherosContainer.css";

const SuperherosContainer = ({ heroes }) => {
    const [index, setIndex] = useState(0);

    const hero = heroes[index];

    return ( 
        <div className='main-container'>
            <Row gutter={16}>
                <Col span={6}>
                    <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                        <HeroImage
                            name={hero.name}
                            src={hero.images.md}
                        />
                        <Navigation
                            onChange={newindex => {
                                setIndex(newindex - 1);
                            }}
                            current={index + 1}
                            total={heroes.length}
                        />
                    </Space>
                </Col>
                <Col span={8}>
                    <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                        <HeroBiography {...hero.biography} />
                        <HeroAppearance {...hero.appearance} />
                    </Space>
                </Col>
                <Col span={10}>
                    <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                        <HeroPowerStats {...hero.powerstats} />
                        <HeroConnections {...hero.connections} />
                    </Space>
                </Col>
            </Row>
        </div>
     );
}
 
export default SuperherosContainer;