import { Spin } from 'antd';
import "./LoadingView.css";

const LoadingView = () => {
    return ( 
        <div className="loading-container">
            <div className="loading-content">
                <Spin tip="Cargando..." />
            </div>
        </div>
     );
}
 
export default LoadingView;