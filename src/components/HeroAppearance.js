import { Card } from 'antd';
import Section from "./Section";

const HeroAppearance = ({
        eyeColor,
        gender,
        hairColor,
        height,
        race,
        weight
    }) => {
    
    return ( 
        <Card title="Appearance" hoverable>
            <Section name="Gender">{gender}</Section>
            <Section name="Hair Color">{hairColor}</Section>
            <Section name="Eye Color">{eyeColor}</Section>
            <Section name="Height">{height[0]} - {height[1]}</Section>
            <Section name="Weight">{weight[0]} - {weight[1]}</Section>
            {race && <Section name="Race">{race}</Section>}
        </Card>
     );
}
 
export default HeroAppearance;