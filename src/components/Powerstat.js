import { Progress } from 'antd';

const Powerstat = ({ name, value }) => {
    return (
        <div className="powerstat">
            <div className="powerstat-name">{name}</div>
            <div className="powerstat-value">
                <Progress percent={value} format={() => value} />
            </div>
        </div>
    );
}
 
export default Powerstat;