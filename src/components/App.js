import React, { useState, useEffect } from 'react';
import axios from "axios";
import LoadingView from "./LoadingView";
import SuperherosContainer from "./SuperherosContainer";
// import superheros from './superheros';

const APIkey = '60a190fc9emsh2018cf6dc7df80fp104dcbjsn290202b4a1ad';
const APIhost = 'superhero-search.p.rapidapi.com';

const App = () => {
    const [heroes, setHeroes] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get('https://superhero-search.p.rapidapi.com/api/heroes', {
            headers: {
                'X-RapidAPI-Key': APIkey,
                'X-RapidAPI-Host': APIhost
            }
        })
        .then(response => {
            console.log(response);
            setHeroes(response.data);
        })
        .catch(error => {
            console.log(error);
        })
        .finally(() => {
            setLoading(false);
        });
    }, []);

    return (loading ? <LoadingView /> : <SuperherosContainer heroes={heroes} />);
}


 
export default App;